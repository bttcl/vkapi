from setuptools import setup

setup(name='vkapi',
      version='0.1.1',
      description='api wrapper for VK',      
      author='Envy',
      author_email='envy@qcrypt.org',
      license='MIT',
      packages=['vkapi'],
      zip_safe=False,
      install_requires = [
      	"requests"
      ]
      )