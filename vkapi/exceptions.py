__author__ = 'envy'

class ConnectionError(Exception):
    pass

class GeneralError(Exception):
    pass

class ApiError(Exception):
    def __init__(self, error_code=None, error_msg=None, request_params=None, payload=None, **kwargs):
        self.error_code = error_code
        self.error_msg = error_msg
        self.request_params = request_params
        self.payload = payload
        self.extra = kwargs
        message = "Code: {code}; Message: {msg}, Params: {params}; Payload: {payload}".format(code=error_code,
                                                                                              msg=error_msg,
                                                                                              params=request_params,
                                                                                              payload=payload)

        super(ApiError, self).__init__(message)



class AuthFailedException(ApiError):
    def __init__(self, *args, **kwargs):
        super(AuthFailedException, self).__init__(*args, **kwargs)
