__author__ = 'envy'
__all__ = ["VkApi"]
import requests
from .exceptions import *
from time import time, sleep
AUTH_FAILED_CODE = 5

class VkApi(object):
    VERSION = "5.53"
    API_URL = "https://api.vk.com"
    MIN_DELAY = 0.34

    request_times = {}

    def __init__(self, session, timeout=8):
        self._session = session
        self.timeout = timeout

    def __getattr__(self, item):
        return VkMethod(self, item)

    def execute(self, method, **kwargs):
        time_since_last_request = time() - self.request_times.get(method, 0)

        if time_since_last_request < self.MIN_DELAY:
            sleep(self.MIN_DELAY - time_since_last_request)


        params = self._session.prepare_request_params(version=self.VERSION,
                                                      api_method=method,
                                                      **kwargs)
        url = "{}/method/{}".format(self.API_URL, method)
        try:
            response = requests.get(url, params, timeout=self.timeout)
            self.request_times[method] = time()
        except requests.RequestException as e:
            raise ConnectionError(e)

        if not response.ok:
            raise ApiError(error_code=-1, error_msg="Strange response", payload={"url": url, "data": params, "status_code": response.status_code})
        try:
            json = response.json()
        except Exception as e:
            raise GeneralError(e)

        if "error" in json:
            err_data = json.get("error")
            e = ApiError(payload=params, **err_data)
            if e.error_code == AUTH_FAILED_CODE:
                raise AuthFailedException(payload=params, **err_data)
            else:
                raise e

        if not "response" in json:
            raise ApiError(error_code=-1, error_msg="Strange response", payload={"url": url, "data": params, "response": json})

        return json.get("response")

    def is_alive(self):
        try:
            self.users.get(user_ids=1)
            return True
        except AuthFailedException:
            return False
        except Exception:
            return None

class VkMethod(object):
    def __init__(self, api, scope):
        self.api = api
        self._scope = scope
        self._method = None

    def __getattr__(self, item):
        self._method = item
        return self

    def __call__(self, **kwargs):
        return self.api.execute(self.method, **kwargs)

    def __repr__(self):
        return "<VkMethod: %s>" % self.method

    @property
    def method(self):
        return ".".join(filter(None, [self._scope, self._method]))
