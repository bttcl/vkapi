from collections import OrderedDict
import requests
from hashlib import md5
from urllib.parse import urlencode
import logging
from .exceptions import *

class DummySession(object):
    def prepare_request_params(self, version, api_method, **kwargs):
        request_params = OrderedDict(v=version)
        request_params.update(kwargs)
        return request_params

class Session(object):
    def __init__(self, access_token, user_id, secret = None):
        self.access_token = access_token
        self.secret = secret
        self.user_id = user_id

    def prepare_request_params(self, version, api_method, **kwargs):
        request_params = OrderedDict(v=version, access_token=self.access_token)
        request_params.update(kwargs)
        query_string = "/method/{}?{}".format(api_method, urlencode(request_params))
        if self.secret:
            sign = md5((query_string + self.secret).encode('utf-8')).hexdigest()
            request_params.update({"sig": sign})

        return request_params

    def __repr__(self):
        return "<{}; Token: {}; UID: {}, Secret: {}>".format(self.__class__.__name__, self.access_token, self.user_id, self.secret)

    def to_dict(self):
        return dict(token=self.access_token, user_id=self.user_id, secret=self.secret)

    @classmethod
    def from_dict(cls, data):
        return cls(access_token=data['token'], user_id=data['user_id'], secret=data['secret'])

class AppSession(Session):
    OAUTH_URL="https://oauth.vk.com/token"
    def __init__(self, *args, **kwargs):
        super(AppSession, self).__init__(*args, **kwargs)

    @classmethod
    def login(cls, login, password, app_id, app_secret, grant_type, scope):
        logger = logging.getLogger("api")
        data = dict(
            username=login,
            password=password,
            grant_type=grant_type,
            client_id=app_id,
            client_secret=app_secret,
            scope=",".join(scope)
        )
        try:
            response = requests.get(cls.OAUTH_URL, data)
        except requests.RequestException as e:
            raise ConnectionError(e)

        if not response.ok:
            logger.error("Url: {url}, ResponseCode: {code}, Params: {params}".format(url=cls.OAUTH_URL, code=response.status_code, params=data))
            return None

        try:
            response_text = response.json()
        except Exception as e:
            logger.exception(e)
            return None

        if "error" in response_text:
            logger.error(response_text.get("error"))
            return None

        access_token = response_text.get("access_token")
        user_id = response_text.get("user_id")
        secret = response_text.get("secret")
        return Session(access_token=access_token, secret=secret, user_id=user_id)


